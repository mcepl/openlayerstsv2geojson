# coding: utf-8
import os
import sys

from setuptools import setup

requires_list = ["geojson"]
if sys.version_info <= (2, 6):
    requires_list.append("unittest2")


def read(fname):
    with open(os.path.join(os.path.dirname(__file__), fname)) as inf:
        return "\n" + inf.read().replace("\r\n", "\n")

setup(
    name='openlayersTSV2GeoJSON',
    version="0.0.1",
    description='Convert TSV files from OpenLayers 2 to GeoJSON',
    author=u'Matěj Cepl',
    author_email='mcepl@cepl.eu',
    url='https://gitlab.com/mcepl/openlayerstsv2geojson',
    scripts=['openlayersTSV2GeoJSON'],
    long_description=read("README.rst"),
    keywords=['OpenLayers', 'GeJSON', 'TSV'],
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.6",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: Implementation :: PyPy",
        "Development Status :: 5 - Production/Stable",
        "Environment :: Console",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
    test_suite="test",
    install_requires=requires_list,
)
